import { Component, OnInit } from '@angular/core';
import {ZinderApiService} from '../../../service/zinder.api.service';
import {ProfilModel} from '../../../model/profil.model';

@Component({
  selector: 'app-liste-profil',
  templateUrl: './liste-profils.component.html',
  styleUrls: ['./liste-profils.component.css']
})
export class ListeProfilsComponent implements OnInit {

  listeProfils: ProfilModel[] = [];

  constructor(
    private zinderApiService: ZinderApiService) {
    this.zinderApiService.getProfilApi().subscribe(profils => {
      this.listeProfils = profils;
  } );
  }

  ngOnInit() {
  }
}
