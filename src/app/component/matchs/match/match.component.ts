import { Component, OnInit } from '@angular/core';
import {ZinderApiService} from '../../../service/zinder.api.service';
import {NewMatchModel} from '../../../model/newMatch.model';
import {ProfilModel} from '../../../model/profil.model';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  userMatchs: NewMatchModel;
  matchProfil: ProfilModel;

  constructor(
    private zinderApiService: ZinderApiService) {
  }

  ngOnInit() {
  }

  getMatch(isMatching: boolean) {
    this.userMatchs.isNewMatching = isMatching;
    this.zinderApiService.postMatchApi(this.matchProfil.id, this.userMatchs).subscribe(match => {
      if (match.isMatching === false) {
        return false;
      } else {
        return true;
      }
    });
  }
  }
