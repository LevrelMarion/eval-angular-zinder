import {Component, Input, OnInit} from '@angular/core';
import {ProfilModel} from '../../../model/profil.model';

@Component({
  selector: 'app-profils',
  templateUrl: './profils.component.html',
  styleUrls: ['./profils.component.css']
})
export class ProfilsComponent implements OnInit {
  @Input() profil: ProfilModel;


  constructor(
  ) {
    }


  ngOnInit() {
  }
}
