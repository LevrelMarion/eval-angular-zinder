import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProfilModel} from '../model/profil.model';
import {map} from 'rxjs/operators';
import {ListeProfilsModel} from '../model/listeProfils.model';
import {NewMatchModel} from '../model/newMatch.model';
import {MatchModel} from '../model/match.model';

@Injectable()
export class ZinderApiService {

  constructor(
    private http: HttpClient
  ) {
  }

  getProfilApi(): Observable<ProfilModel[]> {
    return this.http.get<ListeProfilsModel>('http://localhost:8088/zinder/profils')
      .pipe(map(listProfil => listProfil.profils));
  }

  postMatchApi(id: string, bodyMatch: NewMatchModel): Observable<MatchModel> {
        return this.http.get<MatchModel>(`http://localhost:8088/zinder/profils/${id}/match`, bodyMatch.valueOf());
  }
}
