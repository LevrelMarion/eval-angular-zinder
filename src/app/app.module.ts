import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListeProfilsComponent } from './component/matchs/liste-profils/liste-profils.component';
import { MatchComponent } from './component/matchs/match/match.component';
import { StatistiquesMatchComponent } from './component/statistiques/statistiques-match/statistiques-match.component';
import { StatistiquesProfilComponent } from './component/statistiques/statistiques-profil/statistiques-profil.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import { ProfilsComponent } from './component/matchs/profils/profils.component';
import { ListeStatistiquesComponent } from './component/statistiques/liste-statistiques/liste-statistiques.component';
import {ZinderApiService} from './service/zinder.api.service';

const appRoutes: Routes = [
  { path: '', component: ListeProfilsComponent},
  { path: 'stats', component: ListeStatistiquesComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ListeProfilsComponent,
    MatchComponent,
    StatistiquesMatchComponent,
    StatistiquesProfilComponent,
    ProfilsComponent,
    ListeStatistiquesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes),
  ],
  providers: [ZinderApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
