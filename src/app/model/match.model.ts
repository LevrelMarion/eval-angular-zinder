export class MatchModel {
  id: string;
  profil: string;
  isMatching: boolean;

  constructor(id: string, profil: string, isMatching: boolean) {
    this.id = id;
    this.profil = profil;
    this.isMatching = isMatching;
  }
}
